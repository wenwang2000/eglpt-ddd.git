package com.platform.service.email;
import com.platform.dto.email.EmailGroupDto;
import com.platform.vo.email.EmailGroupVo;
import java.util.List;

/**
 * 邮箱分组 对外服务层
 *
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
public interface EmailGroupService {
	/**
	 * 查询邮箱分组信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 邮箱分组信息
	 */
	EmailGroupVo findEmailGroupById(Integer id, String account);

	/**
	 * 查询邮箱分组列表
	 *
	 * @param emailGroupDto 邮箱分组信息
	 * @return 邮箱分组集合
	 */
	List<EmailGroupVo> findEmailGroupList(EmailGroupDto emailGroupDto);

	/**
	 * 新增邮箱分组
	 *
	 * @param emailGroupDto 邮箱分组信息
	 * @return 结果
	 */
	int saveEmailGroup(EmailGroupDto emailGroupDto);

	/**
	 * 修改邮箱分组
	 *
	 * @param emailGroupDto 邮箱分组信息
	 * @return 结果
	 */
	int modifyEmailGroup(EmailGroupDto emailGroupDto);


	/**
	 * 删除邮箱分组信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 结果
	 */
	int removeEmailGroupById(Integer id, String account);


}
