package com.platform.service.email;
import com.platform.dto.email.EmailAccountDto;
import com.platform.vo.email.EmailAccountVo;
import java.util.List;

/**
 * 电子邮箱 对外服务层
 *
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
public interface EmailAccountService {
	/**
	 * 查询电子邮箱信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 电子邮箱信息
	 */
	EmailAccountVo findEmailAccountById(Integer id, String account);

	/**
	 * 查询电子邮箱列表
	 *
	 * @param emailAccountDto 电子邮箱信息
	 * @return 电子邮箱集合
	 */
	List<EmailAccountVo> findEmailAccountList(EmailAccountDto emailAccountDto);

	/**
	 * 新增电子邮箱
	 *
	 * @param emailAccountDto 电子邮箱信息
	 * @return 结果
	 */
	int saveEmailAccount(EmailAccountDto emailAccountDto);

	/**
	 * 修改电子邮箱
	 *
	 * @param emailAccountDto 电子邮箱信息
	 * @return 结果
	 */
	int modifyEmailAccount(EmailAccountDto emailAccountDto);


	/**
	 * 删除电子邮箱信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 结果
	 */
	int removeEmailAccountById(Integer id, String account);


}
