package com.platform.vo.email;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

/**
 * 电子邮箱  base_email_account 传输对象
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@ApiModel(description ="电子邮箱")
@Data
public class EmailAccountVo implements Serializable
{
	/** ID */
    @ApiModelProperty(value = "ID")
	private Integer id;
	/** 状态(备用) */
    @ApiModelProperty(value = "状态(备用)")
	private String status;
	/** 帐号 */
    @ApiModelProperty(value = "帐号")
	private String account;
	/** 手机 */
    @ApiModelProperty(value = "手机")
	private String mobile;
	/** 名称 */
    @ApiModelProperty(value = "名称")
	private String name;
	/** 机构编码 */
    @ApiModelProperty(value = "机构编码")
	private String comeCode;
	/** 创建者 */
    @ApiModelProperty(value = "创建者")
	private String createBy;
	/** 创建时间 */
    @ApiModelProperty(value = "创建时间")
	private Date createTime;
	/** 更新者 */
    @ApiModelProperty(value = "更新者")
	private String updateBy;
	/** 更新时间 */
    @ApiModelProperty(value = "更新时间")
	private Date updateTime;

}
