package com.platform.domain.repository.email;
import com.platform.domain.entity.email.EmailAccountEntity;
import java.util.List;

/**
 * 电子邮箱 持久化对象方法
 *
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
public interface EmailAccountRepository {
	/**
	 * 查询电子邮箱信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 电子邮箱信息
	 */
	EmailAccountEntity findEmailAccountById(Integer id, String account);

	/**
	 * 查询电子邮箱列表
	 *
	 * @param emailAccountEntity 电子邮箱信息
	 * @return 电子邮箱集合
	 */
	List<EmailAccountEntity> findEmailAccountList(EmailAccountEntity emailAccountEntity);

	/**
	 * 新增电子邮箱
	 *
	 * @param emailAccountEntity 电子邮箱信息
	 * @return 结果
	 */
	int saveEmailAccount(EmailAccountEntity emailAccountEntity);

	/**
	 * 修改电子邮箱
	 *
	 * @param emailAccountEntity 电子邮箱信息
	 * @return 结果
	 */
	int modifyEmailAccount(EmailAccountEntity emailAccountEntity);


	/**
	 * 删除电子邮箱信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 结果
	 */
	int removeEmailAccountById(Integer id, String account);


}
