package com.platform.domain.repository.redis;



import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public interface JedisRepository {
    /**
     * 添加一条记录在Redis数据库中（默认30天）
     *
     * @param key the String 一条记录的key
     *            value the String 一条记录的value
     * @return <code>1</code> 执行成功
     * <code>0</code> 执行失败
     */
    int set(String key, Object value);

    /**
     * 添加一条记录在Redis数据库中
     *
     * @param key the String 一条记录的key
     *            value the String 一条记录的value
     * @return <code>1</code> 执行成功
     * <code>0</code> 执行失败
     */
    int set(String key, Object value, long timeout, TimeUnit timeUnit);

    /**
     * 从Redis数据库中获取一条记录为key的value值
     *
     * @param key the <code>String</code> 要获取记录的key值
     * @return <code>String</code> 执行成功
     * <code>null</code> 执行失败
     */
    Object get(String key);

    String getAsString(String key);

    /**
     * 删除一条记录在Redis数据库中
     *
     * @param key the String 要删除记录的key
     * @return <code>1</code> 执行成功
     * <code>0</code> 执行失败
     */
    int remove(String key);


    int removeWithoutAppkey(String key);

    /**
     * 包含 pattern 字符串的keys 全部删除
     *
     * @param pattern
     * @return
     */
    int removeKeys(String pattern);


    //=========================分布式锁接口=========================//

    /**
     * 尝试锁操作,有默认时间
     *
     * @param _key
     * @return
     */
    boolean tryLock(String _key);

    /**
     * 尝试锁操作,指定默认时间
     *
     * @param _key
     * @param acquireTimeout
     * @param unit
     * @return
     */
    boolean tryLock(String _key, long acquireTimeout, TimeUnit unit);

    /**
     * 锁操作,未获取到锁时,阻塞
     *
     * @param _key
     * @param lockTimeout
     * @param unit
     * @return
     */
    boolean lock(String _key, long lockTimeout, TimeUnit unit);

    /**
     * 解锁操作
     *
     * @param _key
     */
    void unLock(String _key);

    String hget(String key, String field);

    Long hlen(String key);

    Boolean exists(String key);

    Long hset(String key, String field, String value);

    String hmset(String key, Map<String, String> hash);

    Long expire(String key, int seconds);

    List<String> hmget(String key, String... fields);

    Long sadd(String key, String... member);

    Long srem(String key, String... member);

    Long hdel(String key, String field);

    Long zadd(String key, Integer score, String member);
    Long zadd(String key, Double score, String member);

    Double zincrby(String key, Integer score, String member);

    Long zcard(String key);

    Double zscore(String key, String member);

    Long zrevrank(String key, String member);

    Long zrem(String key, String member);


//    List<String> mget(final String... keys);

    Long zremrangeByRank(String key, long start, long end);

    Map<String, String> hgetAll(String _key);

    Long ttl(String key);

    Long llen(String key);

    Long lpush(String key, String item);

    Long lpush(String key, String[] item);

    String lpop(String key) ;
    //start为0表示从第一个元素开始，end为-1表示最后一个元素
    List<String> lrange(String key, int start, int end);

    long incr(String key);
}
