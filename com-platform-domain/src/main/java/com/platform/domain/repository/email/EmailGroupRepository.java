package com.platform.domain.repository.email;
import com.platform.domain.entity.email.EmailGroupEntity;
import java.util.List;

/**
 * 邮箱分组 持久化对象方法
 *
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
public interface EmailGroupRepository {
	/**
	 * 查询邮箱分组信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 邮箱分组信息
	 */
	EmailGroupEntity findEmailGroupById(Integer id, String account);

	/**
	 * 查询邮箱分组列表
	 *
	 * @param emailGroupEntity 邮箱分组信息
	 * @return 邮箱分组集合
	 */
	List<EmailGroupEntity> findEmailGroupList(EmailGroupEntity emailGroupEntity);

	/**
	 * 新增邮箱分组
	 *
	 * @param emailGroupEntity 邮箱分组信息
	 * @return 结果
	 */
	int saveEmailGroup(EmailGroupEntity emailGroupEntity);

	/**
	 * 修改邮箱分组
	 *
	 * @param emailGroupEntity 邮箱分组信息
	 * @return 结果
	 */
	int modifyEmailGroup(EmailGroupEntity emailGroupEntity);


	/**
	 * 删除邮箱分组信息
		
	* @param id ID 		 
	* @param account 帐号  
	 * @return 结果
	 */
	int removeEmailGroupById(Integer id, String account);


}
