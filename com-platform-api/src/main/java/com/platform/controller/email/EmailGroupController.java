package com.platform.controller.email;

import com.platform.bean.ResponseEntity;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import com.platform.controller.BaseController; 
import com.platform.dto.email.EmailGroupDto;
import com.platform.vo.email.EmailGroupVo;
import com.platform.service.email.EmailGroupService;
/**
 * 邮箱分组 信息操作处理
 * 
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Api(tags = "邮箱分组")
@RestController
@RequestMapping("/email/emailGroup")
public class EmailGroupController extends BaseController {

    @Resource
    private EmailGroupService emailGroupService;

    @ApiOperation(value = "查询邮箱分组信息", notes = "", httpMethod = "POST")
    @GetMapping("/findEmailGroupById")
    public ResponseEntity<EmailGroupVo> findEmailGroupById(
                        @ApiParam(name="ID",value="id",required = true) @RequestParam(required = true) Integer id,
                        @ApiParam(name="帐号",value="account",required = true) @RequestParam(required = true) String account,
                        HttpServletRequest request)

    {
        return ResponseEntity.success(emailGroupService.findEmailGroupById(id, account));
    }

    @ApiOperation(value = "查询邮箱分组列表信息", notes = "", httpMethod = "POST")
    @GetMapping("/findEmailGroupList")
    public ResponseEntity<List<EmailGroupVo>> findEmailGroupList(
            @ApiParam(name="邮箱分组",required = false) @RequestBody(required = false) EmailGroupDto EmailGroupDto,
            @ApiParam(name="起始页",value="pageIndex",defaultValue = "1",required = false) @RequestParam(defaultValue = "1",required = false)  Integer pageIndex,
            @ApiParam(name="每页大小",value="pageSize",defaultValue = "20",required = false) @RequestParam(defaultValue = "20",required = false)  Integer pageSize,
                                             HttpServletRequest request)

    {
        startPage();
        PageInfo<EmailGroupVo> pageInfo = new PageInfo<EmailGroupVo>(emailGroupService.findEmailGroupList(EmailGroupDto));
        return ResponseEntity.success(pageInfo.getList(),pageInfo.getTotal(),pageInfo.getSize());
    }

    @ApiOperation(value = "新增邮箱分组信息", notes = "", httpMethod = "POST")
    @PostMapping("/saveEmailGroup")
    public ResponseEntity saveEmailGroup(
            @ApiParam(name="邮箱分组",value = "emailGroup",required = true) @RequestBody(required = true) EmailGroupDto EmailGroupDto,
                                             HttpServletRequest request)

    {
        return ResponseEntity.success(emailGroupService.saveEmailGroup(EmailGroupDto));
    }

    @ApiOperation(value = "修改邮箱分组信息", notes = "", httpMethod = "POST")
    @PostMapping("/modifyEmailGroup")
    public ResponseEntity modifyEmailGroup(
             @ApiParam(name="邮箱分组",value = "emailGroup",required = true) @RequestBody(required = true) EmailGroupDto EmailGroupDto,
                                             HttpServletRequest request)

    {
        return ResponseEntity.success(emailGroupService.modifyEmailGroup(EmailGroupDto));
    }

    @ApiOperation(value = "删除邮箱分组信息", notes = "", httpMethod = "POST")
    @PostMapping("/removeEmailGroupById")
    public ResponseEntity removeEmailGroupById(
        @ApiParam(name="ID",value="id",required = true) @RequestParam(required = true) Integer id,
        @ApiParam(name="帐号",value="account",required = true) @RequestParam(required = true) String account,
                                                     HttpServletRequest request)
    {
        return ResponseEntity.success(emailGroupService.removeEmailGroupById(id, account));
    }
}
