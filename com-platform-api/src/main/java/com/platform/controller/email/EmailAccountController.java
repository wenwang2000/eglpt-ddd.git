package com.platform.controller.email;

import com.platform.bean.ResponseEntity;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

import com.platform.controller.BaseController; 
import com.platform.dto.email.EmailAccountDto;
import com.platform.vo.email.EmailAccountVo;
import com.platform.service.email.EmailAccountService;
/**
 * 电子邮箱 信息操作处理
 * 
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Api(tags = "电子邮箱")
@RestController
@RequestMapping("/email/emailAccount")
public class EmailAccountController extends BaseController {

    @Resource
    private EmailAccountService emailAccountService;

    @ApiOperation(value = "查询电子邮箱信息", notes = "", httpMethod = "POST")
    @GetMapping("/findEmailAccountById")
    public ResponseEntity<EmailAccountVo> findEmailAccountById(
                        @ApiParam(name="ID",value="id",required = true) @RequestParam(required = true) Integer id,
                        @ApiParam(name="帐号",value="account",required = true) @RequestParam(required = true) String account,
                        HttpServletRequest request)

    {
        return ResponseEntity.success(emailAccountService.findEmailAccountById(id, account));
    }

    @ApiOperation(value = "查询电子邮箱列表信息", notes = "", httpMethod = "POST")
    @GetMapping("/findEmailAccountList")
    public ResponseEntity<List<EmailAccountVo>> findEmailAccountList(
            @ApiParam(name="电子邮箱",required = false) @RequestBody(required = false) EmailAccountDto EmailAccountDto,
            @ApiParam(name="起始页",value="pageIndex",defaultValue = "1",required = false) @RequestParam(defaultValue = "1",required = false)  Integer pageIndex,
            @ApiParam(name="每页大小",value="pageSize",defaultValue = "20",required = false) @RequestParam(defaultValue = "20",required = false)  Integer pageSize,
                                             HttpServletRequest request)

    {
        startPage();
        PageInfo<EmailAccountVo> pageInfo = new PageInfo<EmailAccountVo>(emailAccountService.findEmailAccountList(EmailAccountDto));
        return ResponseEntity.success(pageInfo.getList(),pageInfo.getTotal(),pageInfo.getSize());
    }

    @ApiOperation(value = "新增电子邮箱信息", notes = "", httpMethod = "POST")
    @PostMapping("/saveEmailAccount")
    public ResponseEntity saveEmailAccount(
            @ApiParam(name="电子邮箱",value = "emailAccount",required = true) @RequestBody(required = true) EmailAccountDto EmailAccountDto,
                                             HttpServletRequest request)

    {
        return ResponseEntity.success(emailAccountService.saveEmailAccount(EmailAccountDto));
    }

    @ApiOperation(value = "修改电子邮箱信息", notes = "", httpMethod = "POST")
    @PostMapping("/modifyEmailAccount")
    public ResponseEntity modifyEmailAccount(
             @ApiParam(name="电子邮箱",value = "emailAccount",required = true) @RequestBody(required = true) EmailAccountDto EmailAccountDto,
                                             HttpServletRequest request)

    {
        return ResponseEntity.success(emailAccountService.modifyEmailAccount(EmailAccountDto));
    }

    @ApiOperation(value = "删除电子邮箱信息", notes = "", httpMethod = "POST")
    @PostMapping("/removeEmailAccountById")
    public ResponseEntity removeEmailAccountById(
        @ApiParam(name="ID",value="id",required = true) @RequestParam(required = true) Integer id,
        @ApiParam(name="帐号",value="account",required = true) @RequestParam(required = true) String account,
                                                     HttpServletRequest request)
    {
        return ResponseEntity.success(emailAccountService.removeEmailAccountById(id, account));
    }
}
