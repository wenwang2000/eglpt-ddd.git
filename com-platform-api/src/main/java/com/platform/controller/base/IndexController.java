package com.platform.controller.base;


import io.swagger.annotations.ApiOperation;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.platform.controller.BaseController;

import javax.servlet.http.HttpServletRequest;

/***
 * index
 */
@Controller
@RequestMapping("/index")
public class IndexController extends BaseController {

    @ApiOperation(value = "首页", notes = "首页", httpMethod = "GET")
    @GetMapping("/index")
    public String order(HttpServletRequest request, ModelMap modelMap)
    {

        return "index";
    }
}
