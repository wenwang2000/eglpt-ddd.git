package com.platform.common.config;

import com.github.xiaoymin.swaggerbootstrapui.annotations.EnableSwaggerBootstrapUI;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@EnableSwaggerBootstrapUI
/**
 * http://ip:port/swagger-ui.html  -->EnableSwagger2
 * http://ip:port/doc.html    -->EnableSwaggerBootstrapUI
 */
public class Swagger2Config {
    @Bean
    public Docket wxAppDocket() {
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .groupName("eglpt-WxApp")
                .pathMapping("/")
                .select()
                //设置生成的Docket对应的Controller为candidate下的所有Controller
                .apis(RequestHandlerSelectors.basePackage("com.platfrom.controller.wxapp"))
                .paths(PathSelectors.any())
                .build();
    }
    private ApiInfo apiInfo() {
        return new ApiInfoBuilder()
                .title("eglpt-wechat-api")
                .description("eglpt-wechat-api")
                .contact(new Contact("刘立","","472732787@qq.com"))
                .version("1.0")
                .build();
    }

}
