package com.platform.common.component;

import org.springframework.stereotype.Component;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

/**
 * @author liuli
 */
@Component
public class CookieUtils {

    /**
     * @param servletRequest
     * @param cookieName
     * @return
     */
    public String getCookieValue(HttpServletRequest servletRequest, String cookieName) {
        Cookie[] cookies = servletRequest.getCookies();
        if (cookies != null && cookies.length > 0) {
            for (Cookie cookie : cookies) {
                String ckName = cookie.getName();
                if (ckName.equals(cookieName)) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }


}


