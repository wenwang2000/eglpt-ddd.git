package com.platform.bean;


import com.github.pagehelper.PageInfo;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.util.Date;
import java.util.List;

/**
 * @author liuli
 * @param <T>
 */
@ApiModel("返回对象")
public class ResponseEntity<T> {

    @ApiModelProperty(required = true, value = "响应的状态代码")
    private int code;
    @ApiModelProperty(required = true, value = "响应的消息")
    private String msg;
    @ApiModelProperty(required = true, value = "响应时服务器时间戳")
    private Date timestamp;
    @ApiModelProperty(value = "业务数据")
    private T data;
    @ApiModelProperty(value = "分页数据")
    PageInfo<T> pageInfo;
    @ApiModelProperty(value = "总条数")
    private long iTotalRecords;
    @ApiModelProperty(value = "返回条数")
    private long iTotalDisplayRecords;


    public static <T>  ResponseEntity<T> pageInfo(List<T> list) {
        PageInfo<T> pageInfo = new PageInfo<T>(list);
        ResponseEntity<T> entity = success();
        entity.setPageInfo(pageInfo);
        return entity;
    }

    public static <T> ResponseEntity<T> success(T data,long iTotalRecords,long iTotalDisplayRecords) {
        ResponseEntity<T> entity = success();
        entity.setData(data);
        entity.setiTotalDisplayRecords(iTotalDisplayRecords);
        entity.setiTotalRecords(iTotalRecords);
        return entity;
    }


    public static <T> ResponseEntity<T> success() {
        ResponseEntity<T> entity = new ResponseEntity<T>();
        entity.setCode(ResultCode.SUCCESS);
        entity.setMsg(ResultMsg.SUCCESS);
        entity.setTimestamp(new Date());
        return entity;
    }

    public static <T> ResponseEntity<T> success(T data) {
        ResponseEntity<T> entity = success();
        entity.setData(data);
        return entity;
    }

    public static <T> ResponseEntity<T> error() {
        ResponseEntity<T> entity = new ResponseEntity<T>();
        entity.setCode(ResultCode.ERROR);
        entity.setMsg(ResultMsg.ERROR);
        entity.setTimestamp(new Date());
        return entity;
    }

    public static <T> ResponseEntity<T> error(int errorCode) {
        ResponseEntity<T> entity = error();
        entity.setCode(errorCode);
        return entity;
    }

    public static <T> ResponseEntity<T> error(String message) {
        ResponseEntity<T> entity = error();
        entity.setMsg(message);
        return entity;
    }

    public static <T> ResponseEntity<T> successStr(String message) {
        ResponseEntity<T> entity = success();
        entity.setMsg(message);
        return entity;
    }

    public static <T> ResponseEntity<T> error(Integer errorCode, String message) {
        ResponseEntity<T> entity = error();
        entity.setCode(errorCode);
        entity.setMsg(message);
        return entity;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    public long getiTotalRecords() {
        return iTotalRecords;
    }

    public void setiTotalRecords(long iTotalRecords) {
        this.iTotalRecords = iTotalRecords;
    }

    public long getiTotalDisplayRecords() {
        return iTotalDisplayRecords;
    }

    public void setiTotalDisplayRecords(long iTotalDisplayRecords) {
        this.iTotalDisplayRecords = iTotalDisplayRecords;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public PageInfo<T> getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo<T> pageInfo) {
        this.pageInfo = pageInfo;
    }
}
