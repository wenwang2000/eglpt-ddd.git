package com.platform.converter.email;

import com.platform.domain.email.EmailGroupDao;
import com.platform.domain.entity.email.EmailGroupEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 邮箱分组  base_email_group
 * @author 472732787@qq.com
 * @date 2022-04-13
 * 实体对象与dao的转换
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmailGroupEntityConverter
{
        EmailGroupEntityConverter MAPPER = Mappers.getMapper( EmailGroupEntityConverter.class );

        /**
         * entityList to Dao
         * @param emailGroupDaoList
         * @return
         */
        List<EmailGroupEntity> toEntityList(List<EmailGroupDao> emailGroupDaoList);
         /**
         * entity to Dao
         * @param emailGroupEntity
         * @return
         */
        EmailGroupDao toDao(EmailGroupEntity emailGroupEntity);
         /***
         * Dao to entity
         * @param emailGroupDao
         * @return
         */
        EmailGroupEntity toEntity(EmailGroupDao emailGroupDao);
}
