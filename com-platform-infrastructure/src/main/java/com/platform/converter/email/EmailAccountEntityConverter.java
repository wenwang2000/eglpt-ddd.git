package com.platform.converter.email;

import com.platform.domain.email.EmailAccountDao;
import com.platform.domain.entity.email.EmailAccountEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 电子邮箱  base_email_account
 * @author 472732787@qq.com
 * @date 2022-04-13
 * 实体对象与dao的转换
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmailAccountEntityConverter
{
        EmailAccountEntityConverter MAPPER = Mappers.getMapper( EmailAccountEntityConverter.class );

        /**
         * entityList to Dao
         * @param emailAccountDaoList
         * @return
         */
        List<EmailAccountEntity> toEntityList(List<EmailAccountDao> emailAccountDaoList);
         /**
         * entity to Dao
         * @param emailAccountEntity
         * @return
         */
        EmailAccountDao toDao(EmailAccountEntity emailAccountEntity);
         /***
         * Dao to entity
         * @param emailAccountDao
         * @return
         */
        EmailAccountEntity toEntity(EmailAccountDao emailAccountDao);
}
