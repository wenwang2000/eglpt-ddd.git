package com.platform.mapper.email;
import com.platform.domain.email.EmailGroupDao;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
/**
 * 邮箱分组 数据层
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Mapper
public interface EmailGroupMapper {
	/**
     * 查询邮箱分组信息
	
	 * @param id ID 	 
	 * @param account 帐号  
     * @return 邮箱分组信息
     */
	 EmailGroupDao selectEmailGroupById(@Param("id") Integer id, @Param("account") String account);

	/**
     * 查询邮箱分组列表
     *
     * @param emailGroupDao 邮箱分组信息
     * @return 邮箱分组集合
     */
	 List<EmailGroupDao> selectEmailGroupList(EmailGroupDao emailGroupDao);

	/**
     * 新增邮箱分组
     *
     * @param emailGroupDao 邮箱分组信息
     * @return 结果
     */
	 int insertEmailGroup(EmailGroupDao emailGroupDao);

	/**
     * 修改邮箱分组
     *
     * @param emailGroupDao 邮箱分组信息
     * @return 结果
     */
	 int updateEmailGroup(EmailGroupDao emailGroupDao);


	/**
	 * 删除邮箱分组信息
		
	 * @param id ID 		 
	 * @param account 帐号  
	 * @return 结果
	 */
	 int deleteEmailGroupById(@Param("id") Integer id, @Param("account") String account);
	
}