package com.platform.mapper.email;
import com.platform.domain.email.EmailAccountDao;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
/**
 * 电子邮箱 数据层
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Mapper
public interface EmailAccountMapper {
	/**
     * 查询电子邮箱信息
	
	 * @param id ID 	 
	 * @param account 帐号  
     * @return 电子邮箱信息
     */
	 EmailAccountDao selectEmailAccountById(@Param("id") Integer id, @Param("account") String account);

	/**
     * 查询电子邮箱列表
     *
     * @param emailAccountDao 电子邮箱信息
     * @return 电子邮箱集合
     */
	 List<EmailAccountDao> selectEmailAccountList(EmailAccountDao emailAccountDao);

	/**
     * 新增电子邮箱
     *
     * @param emailAccountDao 电子邮箱信息
     * @return 结果
     */
	 int insertEmailAccount(EmailAccountDao emailAccountDao);

	/**
     * 修改电子邮箱
     *
     * @param emailAccountDao 电子邮箱信息
     * @return 结果
     */
	 int updateEmailAccount(EmailAccountDao emailAccountDao);


	/**
	 * 删除电子邮箱信息
		
	 * @param id ID 		 
	 * @param account 帐号  
	 * @return 结果
	 */
	 int deleteEmailAccountById(@Param("id") Integer id, @Param("account") String account);
	
}