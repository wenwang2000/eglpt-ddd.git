package com.platform.repository.email.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.platform.repository.BaseRepository; 
import  com.platform.converter.email.EmailGroupEntityConverter;
import com.platform.domain.entity.email.EmailGroupEntity;
import com.platform.domain.repository.email.EmailGroupRepository;
import com.platform.mapper.email.EmailGroupMapper;
import java.util.List;
/**
 * 邮箱分组 持久化对象方法实现
 * 
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Service
public class EmailGroupRepositoryImpl extends BaseRepository  implements EmailGroupRepository {

    @Resource
    private EmailGroupMapper emailGroupMapper;

    @Override
    public EmailGroupEntity findEmailGroupById(Integer id, String account){
        return EmailGroupEntityConverter.MAPPER.toEntity(emailGroupMapper.selectEmailGroupById(id, account));
    }


    @Override
    public List<EmailGroupEntity> findEmailGroupList(EmailGroupEntity emailGroupEntity){
        return EmailGroupEntityConverter.MAPPER.toEntityList(emailGroupMapper.selectEmailGroupList(EmailGroupEntityConverter.MAPPER.toDao(emailGroupEntity)));
    }

    @Override
    public int saveEmailGroup(EmailGroupEntity emailGroupEntity){
        return emailGroupMapper.insertEmailGroup(EmailGroupEntityConverter.MAPPER.toDao(emailGroupEntity));
    }

    @Override
    public int modifyEmailGroup(EmailGroupEntity emailGroupEntity){
        return emailGroupMapper.updateEmailGroup(EmailGroupEntityConverter.MAPPER.toDao(emailGroupEntity));
    }

    @Override
    public int removeEmailGroupById(Integer id, String account){
        return emailGroupMapper.deleteEmailGroupById( id,  account);
    }

}
