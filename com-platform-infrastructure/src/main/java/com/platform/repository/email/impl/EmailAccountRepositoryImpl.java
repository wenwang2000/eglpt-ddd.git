package com.platform.repository.email.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.platform.repository.BaseRepository; 
import  com.platform.converter.email.EmailAccountEntityConverter;
import com.platform.domain.entity.email.EmailAccountEntity;
import com.platform.domain.repository.email.EmailAccountRepository;
import com.platform.mapper.email.EmailAccountMapper;
import java.util.List;
/**
 * 电子邮箱 持久化对象方法实现
 * 
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Service
public class EmailAccountRepositoryImpl extends BaseRepository  implements EmailAccountRepository {

    @Resource
    private EmailAccountMapper emailAccountMapper;

    @Override
    public EmailAccountEntity findEmailAccountById(Integer id, String account){
        return EmailAccountEntityConverter.MAPPER.toEntity(emailAccountMapper.selectEmailAccountById(id, account));
    }


    @Override
    public List<EmailAccountEntity> findEmailAccountList(EmailAccountEntity emailAccountEntity){
        return EmailAccountEntityConverter.MAPPER.toEntityList(emailAccountMapper.selectEmailAccountList(EmailAccountEntityConverter.MAPPER.toDao(emailAccountEntity)));
    }

    @Override
    public int saveEmailAccount(EmailAccountEntity emailAccountEntity){
        return emailAccountMapper.insertEmailAccount(EmailAccountEntityConverter.MAPPER.toDao(emailAccountEntity));
    }

    @Override
    public int modifyEmailAccount(EmailAccountEntity emailAccountEntity){
        return emailAccountMapper.updateEmailAccount(EmailAccountEntityConverter.MAPPER.toDao(emailAccountEntity));
    }

    @Override
    public int removeEmailAccountById(Integer id, String account){
        return emailAccountMapper.deleteEmailAccountById( id,  account);
    }

}
