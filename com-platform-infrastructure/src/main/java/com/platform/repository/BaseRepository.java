package com.platform.repository;

import com.platform.repository.id.IdGeneratorWorker;
import com.platform.domain.repository.redis.RedisRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * @author liuli31
 * @date 2022/1/19
 * 仓储（repository）:提供查找和持久化对象的方法
 * 仓储父类
 */
public class BaseRepository {
    public Logger log = LoggerFactory.getLogger(this.getClass());
    @Autowired
    RedisRepository redisRepository;
    @Autowired
    IdGeneratorWorker idGeneratorWorker;

}
