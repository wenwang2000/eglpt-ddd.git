package com.platform.domain.email;


import lombok.Data;
import java.util.Date;

/**
 * 邮箱分组  base_email_group 数据对象
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Data
public class EmailGroupDao
{
	/** ID */
	private Integer id;
	/** 状态(备用) */
	private String status;
	/** 帐号 */
	private String account;
	/** 名称 */
	private String name;
	/** 机构编码 */
	private String comeCode;
	/** 创建者 */
	private String createBy;
	/** 创建时间 */
	private Date createTime;
	/** 更新者 */
	private String updateBy;
	/** 更新时间 */
	private Date updateTime;
	/** 收件人 */
	private String receivedEmails;
	/** 抄送 */
	private String ccEmails;

}
