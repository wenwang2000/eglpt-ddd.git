package com.platform.exception;

public class MoneyAmoutNotNullException extends IllegalArgumentException{

    public MoneyAmoutNotNullException(String message) {
        super(message);
    }
}
