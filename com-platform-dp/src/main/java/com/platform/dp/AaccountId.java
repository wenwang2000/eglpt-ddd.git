package com.platform.dp;

/**
 * @author liuli31
 * @date 2022/1/19
 * 帐号ID
 */

public class AaccountId {

    private String value;

    public AaccountId(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
