package com.platform;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


@SpringBootApplication(exclude = {
		DataSourceAutoConfiguration.class
})
//开启缓存 需要在使用的地方加上  @Cacheable
//@EnableCaching


public class EglptDDDApp {
	public  static Logger log = LoggerFactory.getLogger(EglptDDDApp.class);
	public static void main(String[] args) {
		SpringApplication.run(EglptDDDApp.class, args);
		log.info("============app start ......=============");
	}

}
