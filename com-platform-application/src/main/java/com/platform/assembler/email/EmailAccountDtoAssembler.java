package com.platform.assembler.email;

import com.platform.domain.entity.email.EmailAccountEntity;
import com.platform.dto.email.EmailAccountDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 电子邮箱  base_email_account
 * @author 472732787@qq.com
 * @date 2022-04-13
 * dto与实体对象的转换
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmailAccountDtoAssembler
{
        EmailAccountDtoAssembler MAPPER = Mappers.getMapper( EmailAccountDtoAssembler.class );

         /***
         * dto to entity
         * @param emailAccountDto
         * @return
         */
        EmailAccountEntity toEntity(EmailAccountDto emailAccountDto);
}
