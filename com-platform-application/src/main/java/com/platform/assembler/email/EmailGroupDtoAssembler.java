package com.platform.assembler.email;

import com.platform.domain.entity.email.EmailGroupEntity;
import com.platform.dto.email.EmailGroupDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 邮箱分组  base_email_group
 * @author 472732787@qq.com
 * @date 2022-04-13
 * dto与实体对象的转换
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmailGroupDtoAssembler
{
        EmailGroupDtoAssembler MAPPER = Mappers.getMapper( EmailGroupDtoAssembler.class );

         /***
         * dto to entity
         * @param emailGroupDto
         * @return
         */
        EmailGroupEntity toEntity(EmailGroupDto emailGroupDto);
}
