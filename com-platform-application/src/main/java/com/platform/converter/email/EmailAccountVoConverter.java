package com.platform.converter.email;

import com.platform.domain.entity.email.EmailAccountEntity;
import com.platform.vo.email.EmailAccountVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 电子邮箱  base_email_account
 * @author 472732787@qq.com
 * @date 2022-04-13
 * Vo与实体对象的转换
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmailAccountVoConverter
{
        EmailAccountVoConverter MAPPER = Mappers.getMapper( EmailAccountVoConverter.class );

        /**
         * entityList to Vo
         * @param emailAccountEntityList
         * @return
         */
        List<EmailAccountVo> toVoList(List<EmailAccountEntity> emailAccountEntityList);
         /**
         * entity to Vo
         * @param emailAccountEntity
         * @return
         */
        EmailAccountVo toVo(EmailAccountEntity emailAccountEntity);

}
