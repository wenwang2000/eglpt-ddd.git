package com.platform.converter.email;

import com.platform.domain.entity.email.EmailGroupEntity;
import com.platform.vo.email.EmailGroupVo;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 邮箱分组  base_email_group
 * @author 472732787@qq.com
 * @date 2022-04-13
 * Vo与实体对象的转换
 */
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EmailGroupVoConverter
{
        EmailGroupVoConverter MAPPER = Mappers.getMapper( EmailGroupVoConverter.class );

        /**
         * entityList to Vo
         * @param emailGroupEntityList
         * @return
         */
        List<EmailGroupVo> toVoList(List<EmailGroupEntity> emailGroupEntityList);
         /**
         * entity to Vo
         * @param emailGroupEntity
         * @return
         */
        EmailGroupVo toVo(EmailGroupEntity emailGroupEntity);

}
