package com.platform.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author liuli
 */
public class BaseService {
    public  Logger log = LoggerFactory.getLogger(this.getClass());

}
