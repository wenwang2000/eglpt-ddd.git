package com.platform.service.email.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.platform.service.BaseService; 
import com.platform.dto.email.EmailAccountDto;
import com.platform.vo.email.EmailAccountVo;
import com.platform.service.email.EmailAccountService;
import com.platform.domain.repository.email.EmailAccountRepository;
import com.platform.assembler.email.EmailAccountDtoAssembler;
import com.platform.converter.email.EmailAccountVoConverter;
import java.util.List;
/**
 * 电子邮箱 对外服务层实现
 * 
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Service
public class EmailAccountServiceImpl extends BaseService  implements EmailAccountService {

    @Resource
    private EmailAccountRepository emailAccountRepository;


    @Override
    public EmailAccountVo findEmailAccountById(Integer id, String account){
        return EmailAccountVoConverter.MAPPER.toVo(emailAccountRepository.findEmailAccountById(id, account));
    }


    @Override
    public List<EmailAccountVo> findEmailAccountList(EmailAccountDto emailAccountDto){
        return EmailAccountVoConverter.MAPPER.toVoList(emailAccountRepository.findEmailAccountList(EmailAccountDtoAssembler.MAPPER.toEntity(emailAccountDto)));
    }

    @Override
    public int saveEmailAccount(EmailAccountDto emailAccountDto){
        return emailAccountRepository.saveEmailAccount(EmailAccountDtoAssembler.MAPPER.toEntity(emailAccountDto));
    }

    @Override
    public int modifyEmailAccount(EmailAccountDto emailAccountDto){
        return emailAccountRepository.modifyEmailAccount(EmailAccountDtoAssembler.MAPPER.toEntity(emailAccountDto));
    }

    @Override
    public int removeEmailAccountById(Integer id, String account){
        return emailAccountRepository.removeEmailAccountById( id,  account);
    }

}
