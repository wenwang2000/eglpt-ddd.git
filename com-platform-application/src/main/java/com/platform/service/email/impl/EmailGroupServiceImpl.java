package com.platform.service.email.impl;

import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import com.platform.service.BaseService; 
import com.platform.dto.email.EmailGroupDto;
import com.platform.vo.email.EmailGroupVo;
import com.platform.service.email.EmailGroupService;
import com.platform.domain.repository.email.EmailGroupRepository;
import com.platform.assembler.email.EmailGroupDtoAssembler;
import com.platform.converter.email.EmailGroupVoConverter;
import java.util.List;
/**
 * 邮箱分组 对外服务层实现
 * 
 * @author 472732787@qq.com
 * @date 2022-04-13
 */
@Service
public class EmailGroupServiceImpl extends BaseService  implements EmailGroupService {

    @Resource
    private EmailGroupRepository emailGroupRepository;


    @Override
    public EmailGroupVo findEmailGroupById(Integer id, String account){
        return EmailGroupVoConverter.MAPPER.toVo(emailGroupRepository.findEmailGroupById(id, account));
    }


    @Override
    public List<EmailGroupVo> findEmailGroupList(EmailGroupDto emailGroupDto){
        return EmailGroupVoConverter.MAPPER.toVoList(emailGroupRepository.findEmailGroupList(EmailGroupDtoAssembler.MAPPER.toEntity(emailGroupDto)));
    }

    @Override
    public int saveEmailGroup(EmailGroupDto emailGroupDto){
        return emailGroupRepository.saveEmailGroup(EmailGroupDtoAssembler.MAPPER.toEntity(emailGroupDto));
    }

    @Override
    public int modifyEmailGroup(EmailGroupDto emailGroupDto){
        return emailGroupRepository.modifyEmailGroup(EmailGroupDtoAssembler.MAPPER.toEntity(emailGroupDto));
    }

    @Override
    public int removeEmailGroupById(Integer id, String account){
        return emailGroupRepository.removeEmailGroupById( id,  account);
    }

}
